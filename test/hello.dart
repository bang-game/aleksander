class PubScpec {
  String version;
  List<String> _dependencies;

  List<String> get dependencies => _dependencies;

  set dependencies(List<String> value) {
    if (null == value){
      _dependencies = [];
      return ;
    }
    _dependencies = value;
  }

}

main() {
  var pubSpec = PubScpec ();
  pubSpec.version = "1";
  pubSpec._dependencies = ["one", "two"];

  print(pubSpec._dependencies);
  print(pubSpec.version);

}
