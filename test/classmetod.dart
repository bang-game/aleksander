import 'dart:collection';
import 'dart:math';
import 'dart:io';

class Node {
  var data;
  var next;
  var prev;

  Node([this.data, this.next, this.prev]);
}

class LinkedList<T> {
  var head;
  var tail;

  int length = 0;

  @override
  String toString() {
    String Str = "";
    var current = head;
    for (int i = 0; i < length; i++) {
      Str += current.data.toString();
      if (i != length - 1) Str += ", ";
      current = current.next;
    }
    String str = "[$Str]";
    return str;
  }

//  var a2 = new Random();
  add(T data) {
    var current = Node(data);

    if (head == null) {
      head = current;
      tail = current;
    } else {
      current.prev = tail;
      tail.next = current;
      tail = current;
    }
    length++;
  }

  remove(T data) {
    var current = tail;
    while (current != null) {
      if (current.data == data) {
        if (current.prev == null)
          tail = current.prev;
        else
          current.prev.next = current.next;
        if (current.next == null)
          head = current.next;
        else
          current.next.prev = current.prev;
        current.prev = null;
        current.next = null;
        length--;
        break;
      } else
        current = current.prev;
    }
  }

  removeAt(int index) {
    var current = tail;
    int counter = length;
    while (counter != 0) {
      if (counter == index) {
        if (current.prev != null)
          current.prev.next = current.next;
        else
          tail = current.prev;
        if (current.next != null)
          current.next.prev = current.prev;
        else
          head = current.next;
        current.prev = null;
        current.next = null;
        length--;
        break;
      } else {
        current = current.prev;
        counter--;
      }
    }
  }

  setAt(int index, data) {
    var current = tail;
    int counter = length;
    var currentdata = Node(data);
    while (counter != 0) {
      if (counter == index) {
        if (current == head) {
          currentdata.next = head;
          head.prev = currentdata;
          head = currentdata;
          length++;
          break;
        } else {
          currentdata.next = current;
          currentdata.prev = current.prev;
          currentdata.prev.next = currentdata;
          currentdata.next.prev = currentdata;
          length++;
          break;
        }
      } else {
        current = current.prev;
        counter--;
      }
    }
  }
}

void main() {
  LinkedList<int> a = LinkedList<int>();
  a.add(1);
  a.add(2);
  a.add(3);
  a.add(4);
  a.add(5);
  a.remove(2);
  a.removeAt(3);
  a.setAt(3, 2);
  print(a);
  print(a.length);
}
