import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class MyHomePage extends StatelessWidget {
  final String helloText;

  MyHomePage({Key key, this.helloText}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(helloText),
      ),
      body: Stack(
        children: <Widget>[
          Container(
            height: 200,
            width: 200,
            child: Center(child: Text(helloText)),
            color: Colors.blueAccent,
          ),
          Center(
            child: Container(
              height: 100,
              width: 100,
              child: Center(child: Text(helloText)),
              color: Colors.blue,
            ),
          )
        ],
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () async {
          try {
            Response<String> response = await Dio().post(
              "http://192.168.2.139:8081/table/Aleksandr/",
              queryParameters: {
                "order": "Привки, парнишка",
              },
            );
            var json = jsonDecode(response.data);
            if ('error' == json['status']) {
              print(json['text']);
            }
          } catch (e) {
            print(e);
          }
          ;
        },
      ),
    );
  }
}
